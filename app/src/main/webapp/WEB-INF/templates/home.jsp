<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title></title>

<link href="components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

</head>
<body>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-default" role="navigation">
          <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Datamock ITT</a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active form-inline"><a href="#">Tag</a></li>
              <li><a href="#" id="downloadXMI">Download XMI</a></li>
              <li><a href="#" id="uploadMockups" data-toggle="modal" data-target="#uploadZipModal">Upload mockups</a></li>
            </ul>
          </div>
        </nav>

        <div class="panel panel-default" style="background-color: #f8f8f8">
          <div class="panel-body">
            <form role="form" class="form-inline">
              <div class="form-group">
                <label for="mockup-selector"> Mockup: </label> <select id="mockupSelector" class="form-control">
                </select>
                <button class="btn" id="deleteMockup">Delete mockup</button>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div id="uploadZipModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Upload zip</h4>
        </div>
        <div class="modal-body">
          <form id="uploadMockupForm" action="/api/mockups/zipped" method="post" enctype="multipart/form-data">
            <label for="zipfileField">Choose a zipfile</label> <input id="zipfileField" name="zipfile" type="file"></input>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary" onclick="$('#uploadMockupForm')[0].submit()">Upload</button>
        </div>
      </div>
    </div>
  </div>

  <div id="errorListModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Error list</h4>
        </div>
        <div class="modal-body">
          The following validation errors have been found in model:
          <ul id="validationErrorList">
          </ul>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <iframe id="mockupIframe" src="templates/blank.html"></iframe>

  <script src="components/jquery/dist/jquery.min.js"></script>
  <script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="components/underscore/underscore-min.js"></script>
  <script src="components/node-uuid/uuid.js"></script>
  <script src="components/dustjs-linkedin/dist/dust-full.js"></script>

  <script src="js/tagParser.js"></script>
  <script src="js/mockupGenerator.js"></script>
  <script src="js/services.js"></script>
  <script src="js/utils.js"></script>
  <script src="js/xmi.js"></script>
  <script src="js/scripts.js"></script>

</body>
</html>
