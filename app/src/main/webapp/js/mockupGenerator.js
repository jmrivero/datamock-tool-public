DatamockTagProcessor = function() {
	this._classesByName = {};
}

DatamockTagProcessor.prototype.processTags = function(tags, pages, logger) {
		var self = this;
		self.processors.forEach(function(processor) {
			_.each(tags, function(tag) {
				if (processor[tag.tagName]) {
					processor[tag.tagName](tag, pages, logger, self);
				};
			});
		});
		return this.buildModel(logger);
	},

DatamockTagProcessor.prototype.processors = [];

DatamockTagProcessor.prototype.processors[0] = {

	Data: function(tag, pages, logger, self) {
		var dataSpec = self.processDataExpression(tag.parameters);
		if (tag.widget) {
			tag.widget.specs.data = {dataExpression: dataSpec, childDataExpressions: []};
		}
	},

	Delete: function(tag, pages, logger, self) {
		self.processDataExpression(tag.parameters);
	},

	Save: function(tag, pages, logger, self) {
		self.processDataExpression(tag.parameters);
	},

	Transfer: function(tag, pages, logger, self) {
		self.processDataExpression(tag.parameters.dataExpression);
	},

	Associate: function(tag, pages, logger, self) {
		self.processDataExpression(tag.parameters.leftDataExpression);
		self.processDataExpression(tag.parameters.rightDataExpression);
	},

	Dissociate: function(tag, pages, logger, self) {
		self.processDataExpression(tag.parameters.leftDataExpression);
		self.processDataExpression(tag.parameters.rightDataExpression);
	},

	Link: function(tag, pages, logger, self) {
	}

}

DatamockTagProcessor.prototype.processors[1] = {

	Data: function(tag, pages, logger, self) {
		var expression = self.postProcessDataExpression(tag.parameters);
		self.setOperation(expression.effectiveClass, "read");
		//eyes.inspect(expression.effectiveClass);
		if (tag.widget) {
			var dataParents = _.filter(tag.widget.getParents(), function(widget) { return widget.specs.data });
			if (dataParents.length > 0) {
				dataParents[0].specs.data.childDataExpressions.push({widget: tag.widget, dataExpression: tag.widget.specs.data.dataExpression});
				tag.widget.specs.data.parentDataExpression = {widget: dataParents[0], dataExpression: dataParents[0].specs.data.dataExpression};
			}
			if (tag.parameters.dataSource == "this" &&  tag.parameters.class.list) {
				tag.widget.specs.data.bindOnLoading = true;
			}
			if (tag.parameters.class.list) {
				tag.widget.isRepetition = true;
			}
		}
	},

	Delete: function(tag, pages, logger, self) {
		var expression = self.postProcessDataExpression(tag.parameters);
		self.setOperation(expression.effectiveClass, "delete");
		eyes.inspect(expression.effectiveClass)
	},

	Save: function(tag, pages, logger, self) {
		var expression = self.postProcessDataExpression(tag.parameters);
		self.setOperation(expression.effectiveClass, "create");
		eyes.inspect(expression.effectiveClass);
	},

	Transfer: function(tag, pages, logger, self) {
		self.postProcessDataExpression(tag.parameters.dataExpression);
	},

	Associate: function(tag, pages, logger, self) {
		var leftExpression = self.postProcessDataExpression(tag.parameters.leftDataExpression);
		var rightExpression = self.postProcessDataExpression(tag.parameters.rightDataExpression);
		var expression = leftExpression.dataExpression.navigation.length > 0 ? leftExpression : rightExpression;
		self.setAssociateOperation(self.getClassByName(expression.dataExpression.class.name), expression.dataExpression.navigation[0].property, "associate");
	},

	Dissociate: function(tag, pages, logger, self) {
		self.postProcessDataExpression(tag.parameters.leftDataExpression);
		self.postProcessDataExpression(tag.parameters.rightDataExpression);
		var expression = leftExpression.dataExpression.navigation.length > 0 ? leftExpression : rightExpression;
		self.setAssociateOperation(self.getClassByName(expression.dataExpression.class.name), expression.dataExpression.navigation[0].property, "dissociate");
	},

	Link: function(tag, pages, logger, self) {
		if (tag.widget) {
			var selectedPages = _.filter(pages, function(page) { return tag.parameters.pageName == page.name });
			if (selectedPages.length == 0) {
				logger.addEntry("Page not found: " + tag.pageName);
			} else {
				tag.widget.specs.linksTo = selectedPages[0];
			}
		}
	}
}

DatamockTagProcessor.prototype.processors[2] = {

	Save: function(tag, pages, logger, self) {
		if (tag.widget) {
			var dataParents = _.filter(tag.widget.getParents(), function(widget) { return widget.specs.data });
			if (dataParents.length == 0) {
				logger.addEntry("No data context has been defined for Save");
			} else {
				tag.widget.specs.data = {};
				tag.widget.specs.data.saves = {
					dataExpression: dataParents[0].specs.data.dataExpression,
					parentWidgetId: dataParents[0].id,
					childDataExpressions: dataParents[0].specs.data.childDataExpressions
				};
			}
		}
	},

	Transfer: function(tag, pages, logger, self) {

	},

	Delete: function(tag, pages, logger, self) {
		if (tag.widget) {
			var dataParents = _.filter(tag.widget.getParents(), function(widget) { return widget.specs.data });
			if (dataParents.length == 0) {
				logger.addEntry("No data context has been defined for Delete");
			} else {
				tag.widget.specs.data = {};
				tag.widget.specs.data.deletes = {
					dataExpression: dataParents[0].specs.data.dataExpression
				};
			}
		}
	}

}

DatamockTagProcessor.prototype.processDataExpression = function(dataExpression) {
	var self = this;
	if (dataExpression.attribute) {
		var effectiveClass = dataExpression;
		if (dataExpression.navigation.length > 0) {
			effectiveClass = dataExpression.navigation[dataExpression.navigation.length - 1];
		}
		var effectiveClassName = this.processDataHierarchy(effectiveClass.class.name, effectiveClass.class.hierarchy).className;
		this.registerClassAndAttribute(
			effectiveClassName,
			dataExpression.attribute.name,
			dataExpression.attribute.type);
	} else {
		this.processDataHierarchy(dataExpression.class.name, dataExpression.class.hierarchy)
	}
	return dataExpression;
},

DatamockTagProcessor.prototype.postProcessDataExpression = function(dataExpression) {
	var self = this;
	dataExpression.navigation.forEach(function(nav) {
		self.getClassByName(nav.class.name);
	});
	var sourceClass = self.processDataHierarchy(dataExpression.class.name, dataExpression.class.hierarchy);
	dataExpression.navigation.forEach(function(nav) {
		var destinationClass = self.getClassByName(nav.class.name);
		sourceClass.associations[nav.property] = {
			name: nav.property,
			list: nav.class.list,
			optional: nav.class.optional,
			destination: destinationClass
		};
		sourceClass = self.processDataHierarchy(nav.class.name, nav.class.hierarchy);
	});
	return {dataExpression: dataExpression, effectiveClass: sourceClass};
},

DatamockTagProcessor.prototype.processDataHierarchy = function(className, hierarchy) {
	var self = this;
	var currentClass = self.getClassByName(className);
	hierarchy = hierarchy.concat();
	//hierarchy.reverse();
	hierarchy.forEach(function(superclassName) {
		var superclass = self.getClassByName(superclassName);
		currentClass.superclass = superclass;
		currentClass = superclass;
	});
	return currentClass;
},

DatamockTagProcessor.prototype.getClassByName = function(className) {
	var klass = this._classesByName[className];
	if (!klass) {
		this._classesByName[className] = {
			className: className,
			attributes: {},
			associations: {}
		};
		klass = this._classesByName[className];
	}
	return klass;
},

DatamockTagProcessor.prototype.registerClassAndAttribute = function(className, attributeName, attributeType) {
	var klass = this.getClassByName(className);
	if (!klass.attributes[attributeName]) {
		klass.attributes[attributeName] = {
			type: attributeType,
			name: attributeName
		};
	}
},

DatamockTagProcessor.prototype.propertiesToArray = function(object) {
	var a = [];
	for (property in object) {
		a.push(object[property]);
	}
	return a;
},

DatamockTagProcessor.prototype.setOperation = function(klass, operation) {
	if (!klass.operations) {
		klass.operations = {};
	}
	klass.operations[operation] = true;
},

DatamockTagProcessor.prototype.setAssociateOperation = function(klass, property, operationType) {
	if (!klass.associateOperations) {
		klass.associateOperations = {};
	}
	if (!klass.associateOperations[property]) {
		klass.associateOperations[property] = {};
	}
	klass.associateOperations[property][operationType] = true;
},

DatamockTagProcessor.prototype.postProcessClasses = function(logger) {
	for (klassName in this._classesByName) {
		var klass = this._classesByName[klassName];
		for (attributeName in klass.attributes) {
			var superclass = klass.superclass;
			while (superclass) {
				if (superclass.attributes[attributeName]) {
					logger.addEntry('Attribute \'' + attributeName + '\' repeated in hierarchy (classes \'' + klass.className + '\' and \'' + superclass.className + '\')');
				}
				superclass = superclass.superclass;
			}
		}
	}
}

DatamockTagProcessor.prototype.buildModel = function(logger) {
		var self = this;
		this.postProcessClasses(logger);
		var classes = this.propertiesToArray(this._classesByName);
		classes.forEach(function(klass) {
			klass.attributes = self.propertiesToArray(klass.attributes);
			klass.associations = self.propertiesToArray(klass.associations);
		});
		return {
			classes: classes
		};
}
