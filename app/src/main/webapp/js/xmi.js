var xmiDatatypes = {
  "Number": "-84-17--56-5-43645a83:11466542d86:-8000:000000000000087C",
  "Boolean": "-84-17--56-5-43645a83:11466542d86:-8000:0000000000000880"
}
var xmiOtherType = "-84-17--56-5-43645a83:11466542d86:-8000:000000000000087E";

var XMIGenerator = function() {

  function preprocessModel(model) {
    if (!model.id) {
      model.id = uuid.v4();
    }
    ensureIds(model.classes);
    _.each(model.classes, function(aClass) {
      ensureIds(aClass.associations);
      ensureIds(aClass.attributes);
      _.each(aClass.attributes, function(attr) {
        aClass.xmiDatatype = xmiDatatypes[attr.type] || xmiOtherType;
      })
    })
  }

  this.generate = function(model, callback) {
    preprocessModel(model);
    $.get("templates/xmi.dust", function(template) {
      dust.renderSource(template, model, function(err, out) {
        callback(formatXML(out));
      });
    })
  }

}
