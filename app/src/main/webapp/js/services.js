var API_PATH = "/api";

Storage.prototype.setObject = function(key, value) {
  this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function(key, defaultValue) {
  var value = this.getItem(key);
  if (!value) {
    this.setObject(key, defaultValue);
    value = defaultValue;
  }
  return value && JSON.parse(value);
}

var DatamockService = function() {}

DatamockService.prototype.getMockupList = function(callback) {
    $.ajax({
      method: "GET",
      url: API_PATH + "/mockups",
      dataType: "json",
      success : function(result) {
        callback(result);
      }
    });
}

DatamockService.prototype.saveMockup = function(mockupPath, content, callback) {
  $.ajax({
    method: "POST",
    url: API_PATH + "/mockups" + mockupPath,
    data: JSON.stringify(content),
    dataType: "json",
    success : function(result) {
      callback();
    }
  }); 
}

DatamockService.prototype.deleteMockup = function(mockupPath, callback) {
  $.ajax({
    method: "DELETE",
    url: API_PATH + "/mockups" + mockupPath,
    dataType: "json",
    success : function(result) {
      callback();
    }
  }); 
}
