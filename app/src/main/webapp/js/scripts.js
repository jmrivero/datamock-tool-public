
function Logger() {};
Logger.prototype.addEntry = function(entry) {
  currentErrors.push(entry);
}
var logger = new Logger();
var dataSpecs = [];
var model = [];
var service = new DatamockService();
var mockups;
var currentMockup;
var currentErrors = [];

function regenerateDataSpecs() {
  dataSpecs = [];
  _.each(mockups, function(mockup) {
    var wrappedBodyContents = $('<div>' + mockup.content.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/ig, '') + '</div>');
    wrappedBodyContents.find("*[data-datamock-tags]").each(function() {
      var element = $(this);
      var tags = JSON.parse(element.attr("data-datamock-tags"));
      _.each(tags, function(tagContent) {
          var dataSpec = DatamockTagParser.parse(tagContent);
          dataSpecs.push(dataSpec[0]);
      })
    })
   generateModelFromDataSpecs();
  })
}

function openTagEditor(element, parent, changeCallback) {
  var editor = $('<input type="text" />');
  editor.val("Data()");
  editor.css({
    "position": "absolute",
    "left": element.offset().left + 10,
    "top": element.offset(). top + 10 + (parent ? parent.offset().top : 0),
    "width": "100px"
  });
  $("body").append(editor);
  $(editor).blur(function() {
    $(this).remove();
  })
  $(editor).keypress(function(e) {
    if(e.which == 13) {
      if (changeCallback) {
        var result = changeCallback($(this).val());
        if (!result) {
          return;
        }
        $(this).remove();
      }
    }
  });

  editor.focus();
  editor[0].selectionStart = editor[0].selectionEnd = editor.val().length - 1;
}

function addTagContainer(element, parent) {
  var tagContainer = $('<div class="mockupdd-tag-container"><ul></ul></div>');

  setInterval(function() {
    tagContainer.css({
      "position": "absolute",
      "left": element.offset().left + 10 + (parent ? parent.offset().left : 0),
      "top": element.offset(). top + 10 + (parent ? parent.offset().top : 0),
    });
  }, 250);

  $("body").append(tagContainer);

  return tagContainer;
}

function addTagWidget(element, tagContainer, tagContent, onClickCallback) {
  var tag = $('<li>' + tagContent + '</li>');

  $(tagContainer).find('ul').append(tag);

  tag.click(function() {
    // removes tag visually
    $(this).remove();

    // and from model    
    var tags = JSON.parse($(element).attr("data-datamock-tags"));
    tags = _.filter(tags, function(currentTagContent) { return currentTagContent != tagContent });
    $(element).attr("data-datamock-tags", JSON.stringify(tags));

    tagsChanged();

    if (onClickCallback) {
      onClickCallback();
    }
  });

  return tag;
}

function ensureIds(objectList) {
  _.each(objectList, function(object) {
    if (!object.id) {
      object.id = uuid.v4();
    }
  });
}

function generateModelFromDataSpecs() {
  var tagProcessor = new DatamockTagProcessor();
  currentErrors = [];
  model = tagProcessor.processTags(dataSpecs, [], logger);
  if (currentErrors.length > 0) {
    showErrors();
  }
  new XMIGenerator().generate(model, function(xmi) {
    $("#downloadXMI").attr("href", "data:application/octet-stream;charset=utf-16le;base64," + btoa(xmi));
  })
}

function refreshMockupList(callback) {
  service.getMockupList(function(list) {
    mockups = list;
    $("#mockupSelector option").remove();
    $("#mockupSelector").append('<option value=""> (select a mockup) </option>');
    _.each(list, function(mockup) {
      mockup._id = uuid.v4();
      $("#mockupSelector").append('<option value="' + mockup._id + '">' + mockup.path + '</option>');
    })
    if (callback) {
      callback();
    }
  })
}

function saveCurrentMockup() {
  $("#mockupIframe").contents().find("html").find(".mockupdd-selected").removeClass("mockupdd-selected");
  currentMockup.content = "<html>" + $("#mockupIframe").contents().find("html").html() + "</html>";
  service.saveMockup(currentMockup.path, currentMockup.content, function() {
    console.log("Mockup saved");
  })
}

function deleteCurrentMockup() {
  if (!currentMockup) {
    alert("No mockup selected");
  }
  service.deleteMockup(currentMockup.path, function() {
    location.reload();
  })
}

function tagsChanged() {
  saveCurrentMockup();
  regenerateDataSpecs();
}

function enableTagging(element, containerIframe) {
  var selector = "div, input, li, span, label, button";
  $(element).find(selector).each(function() {
    var element = $(this);
    var tagContainer = addTagContainer($(this), $(containerIframe));

    if (!$(element).attr("data-datamock-id")) {
      $(element).attr("data-datamock-id", uuid.v4());
    }

    $(element).mouseover(function(e) {
      e.stopPropagation();
      e.preventDefault();
      $(element).addClass("mockupdd-selected");
    })

    $(element).mouseout(function(e) {
      e.stopPropagation();
      e.preventDefault();
      $(element).removeClass("mockupdd-selected");
    })

    $(element).click(function(e) {
      e.stopPropagation();
      e.preventDefault();
      openTagEditor(element, containerIframe, function(tagContent) {
        try {

          DatamockTagParser.parse(tagContent);
          
          if (!$(element).attr("data-datamock-tags")) {
            $(element).attr("data-datamock-tags", "[]");
          }
          var currentTags = JSON.parse($(element).attr("data-datamock-tags"));
          currentTags.push(tagContent);
          $(element).attr("data-datamock-tags", JSON.stringify(currentTags));

          addTagWidget(element, tagContainer, tagContent);
          tagsChanged();

          return true;

        } catch (exception) {
          alert(exception);
          return false;
        }
      });

    })

    $(element).focus(function(e) {
      e.preventDefault();
    })

    console.log($(element).attr("data-datamock-tags"));

    if ($(element).attr("data-datamock-tags")) {
      _.each(JSON.parse($(element).attr("data-datamock-tags")), function(tagContent) {
        addTagWidget(element, tagContainer, tagContent);
      })
    }

  });
}

function rewriteHead(mockup, wrappedHeadContents) {
  var pathParts = mockup.path.split("/");
  pathParts.pop();
  var mockupFilePath = pathParts.join("/");

  wrappedHeadContents.find("link").each(function() {
    var path = $(this).attr("href");
    if (path[0] != '/') {
      $(this).attr("href", "/mockups" + mockupFilePath + "/" + path);
    }
  });
}

function showErrors() {
  $("#validationErrorList li").remove();
  _.each(currentErrors, function(error) {
    $("#validationErrorList").append("<li>" + error + "</li>")
  });
  $("#errorListModal").modal("show");
}

function findRepeatedAttributesInHierarchy(model) {
  var errors = [];
  _.each(model.classes, function(aClass) {
    _.each(aClass.attributes, function(attr) {
      currentClass = aClass.superclass;
      while (currentClass) {
        if (
          _.filter(currentClass.attributes, function(otherAttr) {
            return otherAttr.name == attr.name
          }).length > 0
        ) {
          //errors.push("Repeated attribute \"" + attr.name + "\" in hierarchy - classes \"" + aClass.cla),
        }
        currentClass = aClass.superclass;
      }
    })
  })
}

$(document).ready(function() {
  refreshMockupList(function(){
    regenerateDataSpecs();
  });

  $("#mockupSelector").change(function() {
    
    $(".mockupdd-tag-container").remove();
    
    var mockupId = $(this).val();
    if (!mockupId) {
      $("#mockupIframe").contents().find("body").html("");
      $("#mockupIframe").contents().find("head").html("");
      currentMockup = null;
      return;
    }

    var styleScript = '<link href="/css/style.css" rel="stylesheet" />';

    var mockup = _.filter(mockups, function(mockup) { return mockup._id == mockupId })[0];
    currentMockup = mockup;
    
    var wrappedBodyContents = $('<div>' + mockup.content.replace(/^[\s\S]*<body.*?>|<\/body>[\s\S]*$/ig, '') + '</div>');
    var wrappedHeadContents = $('<div>' + mockup.content.replace(/^[\s\S]*<head.*?>|<\/head>[\s\S]*$/ig, '') + styleScript + '</div>');

    rewriteHead(mockup, wrappedHeadContents);

    wrappedBodyContents.find("script").remove();
    wrappedHeadContents.find("script").remove();

    $("#mockupIframe").contents().find("body").html(wrappedBodyContents.children());
    $("#mockupIframe").contents().find("head").html(wrappedHeadContents.children());

    enableTagging($("#mockupIframe").contents(), $("#mockupIframe"));
  }); 
   
  $("#saveModel").click(function() {
    saveModel();
  })

  $("#deleteMockup").click(function(e) {
    e.preventDefault();
    deleteCurrentMockup();
  });

})


