package org.mockupdd.datamock.rest;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.mockupdd.datamock.service.MockupService;

import com.sun.jersey.multipart.FormDataParam;

@Path("/mockups")
public class MockupResource {
  
  private MockupService mockupService;

  public MockupResource(MockupService mockupService) {
    super();
    this.mockupService = mockupService;
  }

  @GET
  @Path("/")
  @Produces(MediaType.APPLICATION_JSON)
  public String getMockups() {
    return this.mockupService.getMockups();
  }
  
  @POST
  @Path("/")
  @Produces(MediaType.APPLICATION_JSON)
  public String saveMockups(String content) {
    this.mockupService.saveMockups(content);
    return "{\"result\": \"OK\"}";
  }
  
  @POST
  @Path("/{mockupPath : .+}")
  @Produces(MediaType.APPLICATION_JSON)
  public String saveMockups(String content, @PathParam("mockupPath") String mockupId) {
    Boolean result = this.mockupService.saveMockup(mockupId, content);
    if (result) {
      return "{\"result\": \"OK\"}";
    } else {
      return "{\"result\": \"ERROR\"}";
    }
  }
  
  @DELETE
  @Path("/{mockupPath : .+}")
  @Produces(MediaType.APPLICATION_JSON)
  public String deleteMockup(String content, @PathParam("mockupPath") String mockupId) {
    Boolean result = this.mockupService.deleteMockup(mockupId);
    if (result) {
      return "{\"result\": \"OK\"}";
    } else {
      return "{\"result\": \"ERROR\"}";
    }
  }
  
  @POST
  @Path("/zipped")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Response saveZippedMockups(@FormDataParam("zipfile") InputStream content) throws URISyntaxException {
    this.mockupService.saveZippedMockups(content);
    return Response.temporaryRedirect(new URI("../")).build();
  }
  

}
