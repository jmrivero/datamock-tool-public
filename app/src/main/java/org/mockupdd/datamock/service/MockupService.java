package org.mockupdd.datamock.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.mockupdd.datamock.model.dto.MockupDTO;

public class MockupService {

  private String filePath;
  private String resourcePath;
  private String tempDir;

  public MockupService(String filePath, String resourcePath, String tempDir) {
    super();
    this.filePath = filePath;
    this.resourcePath = resourcePath;
    this.tempDir = tempDir;
  }

  public void saveMockups(String content) {
    try {
      FileUtils.writeStringToFile(new File(filePath), content);
    } catch (IOException e) {
      // TODO logging
    }
  }

  public Boolean saveMockup(String mockupPath, String content) {
    try {
      File mockupFile = new File(filePath + "/" + mockupPath);
      content = content.replace("\\\"", "\"");
      content = content.replace("\\n", "\n");
      content = content.replace("\\t", "\t");
      if (mockupFile.exists()) {
        FileUtils.writeStringToFile(new File(filePath + "/" + mockupPath), content);
        return true;
      }
    } catch (IOException e) {
      // TODO logging
    }
    return false;
  }

  public String getMockups() {
    try {
      List<MockupDTO> mockups = new ArrayList<MockupDTO>();
      File directory = new File(filePath);

      this.getMockupsFromDirectory(mockups, directory, directory);

      return serialize(mockups);
    } catch (IOException e) {
      // TODO logging
      return null;
    }

  }

  private void getMockupsFromDirectory(List<MockupDTO> mockups, File directory, File rootDirectory) throws IOException {
    File[] files = directory.listFiles();
    for (File file : files) {
      if (file.isFile()) {
        if (file.getName().endsWith(".html")) {
          String relativePath = file.getAbsolutePath().substring(new Long(rootDirectory.getAbsolutePath().length()).intValue());
          mockups.add(new MockupDTO(relativePath, FileUtils.readFileToString(file)));
        }
      } else {
        this.getMockupsFromDirectory(mockups, file, rootDirectory);
      }
    }
  }

  public void saveZippedMockups(InputStream content) {
    try {
      String filename = UUID.randomUUID().toString();
      File zipfile = new File(this.tempDir + "/" + filename);
      
      FileOutputStream fos = new FileOutputStream(zipfile);
      IOUtils.copy(content, fos);
      fos.close();
      
      new ZipFile(zipfile).extractAll(this.filePath);
    } catch (Exception e) {
      // TODO logging
    }

  }

  private String serialize(Object object) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(object);
      return json;
    } catch (Exception e) {
      // TODO logging
      return null;
    }

  }

  public Boolean deleteMockup(String mockupPath) {
    File mockupFile = new File(filePath + "/" + mockupPath);
    if (mockupFile.exists()) {
      mockupFile.delete();
    } else {
      // TODO logging
      return false;
    }
    return true;
  }

}
