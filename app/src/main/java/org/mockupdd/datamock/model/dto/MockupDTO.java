package org.mockupdd.datamock.model.dto;

public class MockupDTO {

  private String path;
  private String content;

  public MockupDTO(String path, String content) {
    super();
    this.path = path;
    this.content = content;
  }

  public MockupDTO() {
    super();
  }

  public String getPath() {
    return path;
  }

  public void setPath(String id) {
    this.path = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

}
