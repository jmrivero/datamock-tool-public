cd ./app/src/main/webapp
bower install
cd ../../../..
if [ ! -x app/src/main/webapp/mockups ]; then
	cp -R config/initial-mockups app/src/main/webapp/mockups
fi
if [ ! -x temp ]; then
	mkdir temp
fi
export MAVEN_OPTS="-Xmx256m -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8008,server=y,suspend=n"
mvn jetty:run -Djetty.port=8080 -f app/pom.xml

